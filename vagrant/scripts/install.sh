#!/bin/bash

update_host_file()
{         

    echo -e "\033[4;33mUpdating host ${2} in /etc/hosts\033[0m\n"
    sed -e "/^.*ubuntu-.*/d" -i /etc/hosts
    sed -e "s/^.*${2}.*/${1} ${2} ${2}.local/" -i /etc/hosts
    sed -i -e 's/#DNS=/DNS=8.8.8.8/' /etc/systemd/resolved.conf
    service systemd-resolved restart
}

install_docker_ce()
{
    echo -e "\033[4;33mInstalling Docker CE\033[0;0m" # green
    if `test -f /vagrant/data/packages.tar`; then
        echo -e "\033[0;32mOffline Packages Found\033[0;0m" # green
        echo -e "\033[0;32mExtracting and installing.\033[0;0m" # green

        cd / && tar -xf /vagrant/data/packages.tar

        dpkg -i /tmp/packages/*.deb >> /dev/null 2>&1
        echo -e "\033[1;32mDocker Installed\033[0;36m"
        docker version
        echo -e "\033[0m"
    else
        echo -e "\033[1;31mNo Offline Packages Found\033[0m"
        echo -e "\033[0;32mDownloading and installing. It might take upto 5 minutes.\033[0;0m" # green

        date
        curl -fsSL https://get.docker.com -o /tmp/get-docker.sh 2>&1
        sh /tmp/get-docker.sh > /dev/null 2>&1

        echo -e "\033[1;32mDocker Installed\033[0;0m"
        docker version

        mkdir /tmp/packages

        echo -e "\n\033[0;32mSaving packages for future reuse.\033[0;0m" # green
        
        find /var/cache/apt/archives/ -name 'docker*.deb' -exec cp "{}" /tmp/packages \;    > /dev/null 2>&1
        find /var/cache/apt/archives/ -name 'container*.deb' -exec cp "{}" /tmp/packages \; > /dev/null 2>&1

        tar -cvf /tmp/packages.tar /tmp/packages            > /dev/null 2>&1
        cp /tmp/packages.tar /vagrant/data/packages.tar     > /dev/null 2>&1

    fi
    
    

}

install_cri_docker()
{
    echo -e "\033[4;33mInstalling CRI (cri-docker)\033[0m"

    if `test -f /vagrant/data/cri-dockerd`; then
        echo -e "\033[0;32mOffline Binaries Found\033[0m"
        echo -e "\033[0;32mInstalling and configuring.\033[0m"
        mkdir -p /usr/local/bin
        install -o root -g root -m 0755 /vagrant/data/cri-dockerd /usr/local/bin/cri-dockerd

        cp -a /vagrant/data/cri-docker.s* /etc/systemd/system
        sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
        systemctl daemon-reload
        systemctl enable cri-docker.service         > /dev/null 2>&1
        systemctl enable --now cri-docker.socket    > /dev/null 2>&1
        echo -e "\033[1;32mCRI-Dockerd Installed\033[0m"
   
    else
        echo -e "\033[1;31mNo Offline Binaries Found\033[0m"
        echo -e "\033[0;32mDownloading and building. It might take upto 5 minutes.\033[0m"

        date
        git clone https://github.com/Mirantis/cri-dockerd.git  /tmp/cri-dockerd  > /dev/null 2>&1
        curl -fsSL https://storage.googleapis.com/golang/getgo/installer_linux -o /tmp/installer_linux
        chmod +x /tmp/installer_linux
        cd /tmp && ./installer_linux    > /dev/null 2>&1
        source /root/.bash_profile 
        cd /tmp/cri-dockerd && mkdir bin && go build -o bin/cri-dockerd && cp bin/cri-dockerd /vagrant/data/cri-dockerd
        date
        mkdir -p /usr/local/bin
        install -o root -g root -m 0755 /vagrant/data/cri-dockerd /usr/local/bin/cri-dockerd
        cp -a /tmp/cri-dockerd/packaging/systemd/* /etc/systemd/system
        cp -a /tmp/cri-dockerd/packaging/systemd/* /vagrant/data
        sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
        systemctl daemon-reload
        systemctl enable cri-docker.service
        systemctl enable --now cri-docker.socket
        cat ~/.bash_profile >> ~/.bashrc
        rm ~/.bash_profile

        echo -e "\033[1;32mCRI-Dockerd Installed\033[0m"
        echo -e "\033[0;32mSaving cri-dockerd binary for future reuse.\033[0m\n"
    fi


}                

k8s_prequisite()
{                  
    echo -e "\033[4;33mUpdating OS settings for kubernetes\033[0m"

    echo overlay        >> /etc/modules-load.d/k8s.conf
    echo br_netfilter   >> /etc/modules-load.d/k8s.conf
        
    modprobe overlay
    modprobe br_netfilter
                
    echo net.bridge.bridge-nf-call-iptables  = 1 >> /etc/sysctl.d/k8s.conf
    echo net.bridge.bridge-nf-call-ip6tables = 1 >> /etc/sysctl.d/k8s.conf
    echo net.ipv4.ip_forward                 = 1 >> /etc/sysctl.d/k8s.conf

        
    sudo sysctl --system > /dev/null 2>&1
}

k8s_components()
{

    echo -e "\033[4;33mInstalling kubeadm kubectl kubelet for verion ${3}\033[0m"
    curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
    echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

    if [[ $4 == "master" ]]; then

        apt-get update > /dev/null 2>&1 
        apt-get install cockpit cockpit-pcp kubelet=${3} kubeadm=${3} kubectl=${3} -y > /dev/null 2>&1
        
        systemctl enable cockpit.socket
        echo -e "\033[0;32mEnabling cockpit\033[0m"
        echo -e "\033[1;32mInitiating kubernetes cluster control-plane\033[0m"

        kubeadm init --cri-socket unix:///var/run/cri-dockerd.sock --apiserver-advertise-address $1 --pod-network-cidr 192.168.100.0/16 | tee /vagrant/logs/current_master.log

        export KUBECONFIG=/etc/kubernetes/admin.conf
        echo export KUBECONFIG=/etc/kubernetes/admin.conf >> ~/.bashrc

        echo -e "\033[1;32mApplying calico network add-on\033[0m"
        curl -fsSL https://raw.githubusercontent.com/projectcalico/calico/v3.24.5/manifests/calico.yaml -o /tmp/calico.yaml

        kubectl apply -f /tmp/calico.yaml

        echo -e "\033[1;32mKubernetes pods list\033[0m"
        kubectl get pods -A -o wide

        echo -e "\033[1;32mKubernetes nodes list\033[0m"
        kubectl get nodes -A -o wide

        echo -e "\033[1;32mControl-plane Initialization Completed\033[0m"

        export KUBECONFIG=/etc/kubernetes/admin.conf
        kubectl completion bash >> ~/.bashrc

        echo kubeadm join 192.168.56.10:6443 --token `kubeadm token list -o yaml | grep token: | cut -d ' ' -f2` --discovery-token-ca-cert-hash sha256:`openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null |    openssl dgst -sha256 -hex | sed 's/^.* //'` --cri-socket unix:///var/run/cri-dockerd.sock > /vagrant/scripts/join_cluster.sh

    else
        apt-get update  > /dev/null 2>&1
        apt-get install -y kubelet=${3} kubeadm=${3} -y > /dev/null 2>&1
        echo -e "\033[0;32mJoining the cluster.\033[0m"

        bash /vagrant/scripts/join_cluster.sh > /dev/null 2>&1
    fi
}

echo -e "\033[4;33mRunning Scripts\033[0m"
echo "Inputs"
echo IP=$1 Hostname=$2 KubernetesVersion=$3 VM=$4
update_host_file $1 $2
install_docker_ce
install_cri_docker
k8s_prequisite
k8s_components $1 $2 $3 $4

echo -e "\033[4;33mScript executed\033[0m"
