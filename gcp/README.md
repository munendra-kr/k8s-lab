## Pupose
### The quick way to instantitate the VMs in GCP.

#### Prequisite
*   Gitbash should be installed for windows.
*   Visual Studio Code (For handy operation)

#### Procedure
1. Create a ecdsa keypair
```shell
# Open git bash in windows and run the following command. Press enter few times to continue with default values.
ssh-keygen -t ecdsa

# Get the content of your ecdsa public key and copy it.
cat ~/.ssh/id_ecdsa.pub
ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBMInW8UCS5grCqK9YV9LEuk/JSCquBp17P3j0GbWhrd/rPOv2PgscMMuDDRpcQF8i9jCgBE0958RtbbCHE61klo= username@hostname
```

2. Login to GCP
3. Go to navigation menu -> Compute Engine -> Metadata (_Under settings_)
4. Go to SSH KEYS
5. Click on **edit** -> **add item**
6. Add the content of your public ecdsa key, **in end** edit the username to gcp/gmail userid which you are using to login GCP.
7. Instantiate the VM with GUI or with **CLI**.
8. Deploy VM using gcloud cli. You can open cli in top right second ico in GCP page.

```shell
export PROJECT_NAME=<your project name>

gcloud compute instances create master-01 --project=${PROJECT_NAME} --zone=asia-south2-a --machine-type=e2-medium --network-interface=network-tier=PREMIUM,subnet=default --maintenance-policy=MIGRATE --provisioning-model=STANDARD --service-account=114138824924-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/cloud-platform --enable-display-device --tags=http-server,https-server --create-disk=auto-delete=yes,boot=yes,device-name=master01,image=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20221101a,mode=rw,size=10,type=projects/${PROJECT_NAME}/zones/asia-south2-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any

gcloud compute instances create worker-02 --project=${PROJECT_NAME} --zone=asia-south2-a --machine-type=e2-medium --network-interface=network-tier=PREMIUM,subnet=default --maintenance-policy=MIGRATE --provisioning-model=STANDARD --service-account=114138824924-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/cloud-platform --enable-display-device --tags=http-server,https-server --create-disk=auto-delete=yes,boot=yes,device-name=worker01,image=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20221101a,mode=rw,size=10,type=projects/${PROJECT_NAME}/zones/asia-south2-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any

gcloud compute instances create worker-03 --project=${PROJECT_NAME} --zone=asia-south2-a --machine-type=e2-medium --network-interface=network-tier=PREMIUM,subnet=default --maintenance-policy=MIGRATE --provisioning-model=STANDARD --service-account=114138824924-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/cloud-platform --enable-display-device --tags=http-server,https-server --create-disk=auto-delete=yes,boot=yes,device-name=worker02,image=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20221101a,mode=rw,size=10,type=projects/${PROJECT_NAME}/zones/asia-south2-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any

# To check the VMs and their global ip.
gcloud compute instances list


```
9. Once the VM(s) are instantiated. You can login from your pc.
```shell
ssh your_user_id@vm_global_ip
# or
ssh -i ~/.ssh/id_ecdsa your_user_id@vm_global_ip
```